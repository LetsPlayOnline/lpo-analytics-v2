package eu.letsplayonline.analytics.core.test;

import java.util.Random;
import java.util.UUID;

import eu.letsplayonline.analytics.core.Analytics;
import eu.letsplayonline.analytics.core.IAnalytics;
import eu.letsplayonline.analytics.core.util.Entity;
import eu.letsplayonline.analytics.core.util.EntityType;

public class ExampleCall {

	public static void main(String[] args) throws InterruptedException {
		if (args == null || args.length == 0) {
			System.out.println("Did not find analytics ID to use.");
			return;
		}
		String uaId = args[0];

		System.out.println("Using " + uaId + " for this test.");
		Analytics.configure("djg-test", uaId);
		Analytics.start();
		IAnalytics instance = Analytics.getInstance();
		for (int i = 0; i < 1000; i++) {
			UUID uuid = UUID.randomUUID();
			Entity entity = new Entity(uuid, "DJGummikuh", getIP(),
					"Point of no entry", "1.6.4", EntityType.PLAYER);
			instance.join(entity);
			Thread.sleep(10);
			int numberOfLines = r.nextInt(14);
			for (int j = 0; j < numberOfLines; j++) {
				instance.talk(entity, "asdasd", (r.nextBoolean() ? "global"
						: "nation"));
			}
			numberOfLines = r.nextInt(14);
			for (int j = 0; j < numberOfLines; j++) {
				instance.whisper(entity, (r.nextBoolean() ? "DJGummikuh"
						: "Paktosan"));
			}
			instance.die(entity, (r.nextBoolean() ? "Fell off the world"
					: "Was killed by a giant rabbit"));
			instance.changeServer(entity, "Point of no return");
			instance.changeWorld(entity, "Disneyworld");
			instance.leave(entity);
			Thread.sleep(10);
		}
		Analytics.stop();
		System.out.println("Finished");
	}

	private static final Random r = new Random(System.currentTimeMillis());

	private static String getIP() {
		StringBuilder sb = new StringBuilder(15);
		long ip = r.nextLong();
		for (int i = 0; i < 4; i++) {
			sb.insert(0, Long.toString(ip & 0xff));

			if (i < 3) {
				sb.insert(0, '.');
			}

			ip >>= 8;
		}

		return sb.toString();
	}
}
