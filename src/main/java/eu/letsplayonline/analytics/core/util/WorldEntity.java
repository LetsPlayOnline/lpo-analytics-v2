package eu.letsplayonline.analytics.core.util;

import java.util.UUID;

/**
 * This class represents the Entity on the single server scope (Where location
 * is not the server, but the world).
 * 
 * @author Johannes Frank
 * 
 */
public class WorldEntity extends Entity {

	/**
	 * The constructor for the world Entity.
	 * 
	 * @param id
	 *            - the players unique id
	 * @param name
	 *            - the name of the player
	 * @param host
	 *            - the endpoint of the player
	 * @param locationname
	 *            - the world the player is on
	 * @param type
	 *            - the entity type.
	 * @param clientVersion
     *            - the version of the players client
	 */
	public WorldEntity(UUID id, String name, String host, String locationname,
			String clientVersion, EntityType type) {
		super(id, name, host, locationname, clientVersion, type);
	}

	@Override
	public String getLocationNameAsUrl() {
		return "/world/" + getLocationName();
	}

	@Override
	public String getLocationNameAugmented() {
		return "World - " + getLocationName();
	}
}
