package eu.letsplayonline.analytics.core.internal.events;

import eu.letsplayonline.analytics.core.internal.AnalyticsImpl;
import eu.letsplayonline.analytics.core.util.Entity;
import eu.letsplayonline.analytics.core.util.Values;

/**
 * This class handles when a new player joins the server (network).
 * 
 * @author Johannes Frank
 * 
 */
public class JoinEvent extends EventBase {

	public JoinEvent(Entity whoJoined, AnalyticsImpl base) {
		super(whoJoined, base);
		addParam(Values.HIT_TYPE, "event");
		addParam(Values.EVENT_CATEGORY, "Player Connection");
		addParam(Values.EVENT_ACTION, "Join");
		addParam(Values.EVENT_LABEL, "Player Join");
		addParam(Values.SESSION_CONTROL, "start");
	}

}
