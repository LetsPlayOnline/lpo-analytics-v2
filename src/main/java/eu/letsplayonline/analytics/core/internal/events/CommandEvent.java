/**
 * 
 */
package eu.letsplayonline.analytics.core.internal.events;

import eu.letsplayonline.analytics.core.internal.AnalyticsImpl;
import eu.letsplayonline.analytics.core.util.Entity;
import eu.letsplayonline.analytics.core.util.Values;

/**
 * This class handles events when a player dispatches a command.
 * 
 * @author Johannes Frank
 * 
 */
public class CommandEvent extends EventBase {

	public CommandEvent(Entity entity, String command, AnalyticsImpl base) {
		super(entity, base);
		addParam(Values.HIT_TYPE, "event");
		addParam(Values.EVENT_CATEGORY, "Player Command");
		addParam(Values.EVENT_ACTION, command);
		addParam(Values.EVENT_LABEL, "Player Command");
	}

}
