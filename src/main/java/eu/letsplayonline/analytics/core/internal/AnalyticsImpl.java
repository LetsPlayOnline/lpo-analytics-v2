package eu.letsplayonline.analytics.core.internal;

import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import eu.letsplayonline.analytics.core.IAnalytics;
import eu.letsplayonline.analytics.core.internal.events.CommandEvent;
import eu.letsplayonline.analytics.core.internal.events.DieEvent;
import eu.letsplayonline.analytics.core.internal.events.JoinEvent;
import eu.letsplayonline.analytics.core.internal.events.LeaveEvent;
import eu.letsplayonline.analytics.core.internal.events.OnlineEvent;
import eu.letsplayonline.analytics.core.internal.events.TalkEvent;
import eu.letsplayonline.analytics.core.internal.events.WhisperEvent;
import eu.letsplayonline.analytics.core.util.Entity;
import eu.letsplayonline.analytics.core.util.WorldEntity;

/**
 * Implementation of the analytics interface. Does the actual communication with
 * Google.
 * 
 * @author Johannes Frank
 * 
 */
public class AnalyticsImpl implements IAnalytics {

	/**
	 * @return the hostname
	 */
	public String getHostname() {
		return hostname;
	}

	/**
	 * @return the trackerId
	 */
	public String getTrackerId() {
		return trackerId;
	}

	/**
	 * Contains all currently tracked Entities.
	 */
	private Map<UUID, Entity> trackedEntities = new ConcurrentHashMap<>();

	/**
	 * The Thread pool for this instance.
	 */
	private final ExecutorService es;

	/**
	 * scheduled executor service is used for online duration measurement.
	 */
	private final ScheduledExecutorService ses;

	/**
	 * the hostname this instance is running for.
	 */
	private final String hostname;

	/**
	 * the google analytics tracker ID to use.
	 */
	private final String trackerId;

	/**
	 * Constructor.
	 * 
	 * @param hostname
	 *            - the hostname to use this instance for.
	 * @param trackerId
	 *            - the Tracker ID from Google Analytics.
	 */
	public AnalyticsImpl(String hostname, String trackerId) {
		this.hostname = hostname;
		this.trackerId = trackerId;
		es = Executors.newFixedThreadPool(1);
		ses = Executors.newScheduledThreadPool(1);
		ses.scheduleAtFixedRate(new OnlineMonitor(), 60, 60, TimeUnit.SECONDS);
	}

	/**
	 * Shuts down this instance (e.g. stops the threadpool).
	 */
	public void shutdown() {
		es.shutdown();
		ses.shutdown();
	}

	@Override
	public void join(Entity entity) {
		if (!trackedEntities.containsKey(entity.getId())) {
			trackedEntities.put(entity.getId(), entity);
			es.submit(new JoinEvent(entity, this));
			es.submit(new OnlineEvent(entity, this));
		}
	}

	@Override
	public void leave(Entity entity) {
		trackedEntities.remove(entity.getId());
		es.submit(new LeaveEvent(entity, true, null, this));

	}

	@Override
	public void kick(Entity entity, String reason) {
		trackedEntities.remove(entity.getId());
		es.submit(new LeaveEvent(entity, false, reason, this));

	}

	@Override
	public void talk(Entity entity, String text, String channel) {
		es.submit(new TalkEvent(entity, text, channel, this));

	}

	@Override
	public void whisper(Entity entity, String to) {
		es.submit(new WhisperEvent(entity, to, this));
	}

	@Override
	public void die(Entity entity, String cause) {
		es.submit(new DieEvent(entity, cause, this));
	}

	@Override
	public void changeServer(Entity entity, String targetServer) {
		trackedEntities.put(
				entity.getId(),
				new Entity(entity.getId(), entity.getName(), entity.getHost(),
						targetServer, entity.getClientVersion(), entity
								.getType()));
		es.submit(new OnlineEvent(trackedEntities.get(entity.getId()), this));

	}

	@Override
	public void changeWorld(Entity entity, String targetWorld) {
		trackedEntities.put(
				entity.getId(),
				new WorldEntity(entity.getId(), entity.getName(), entity
						.getHost(), targetWorld, entity.getClientVersion(),
						entity.getType()));
		es.submit(new OnlineEvent(trackedEntities.get(entity.getId()), this));

	}

	@Override
	public void command(Entity entity, String command) {
		es.submit(new CommandEvent(entity, command, this));

	}

	/**
	 * Private class that is scheduled every minute and sends alive events for
	 * every player currently connected.
	 * 
	 * @author Johannes Frank
	 * 
	 */
	private class OnlineMonitor implements Runnable {
		@Override
		public void run() {
			for (Entity entity : trackedEntities.values()) {
				es.submit(new OnlineEvent(entity, AnalyticsImpl.this));
			}
		};
	}

}
