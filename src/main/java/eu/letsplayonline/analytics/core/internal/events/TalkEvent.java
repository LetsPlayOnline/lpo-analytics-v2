/**
 * 
 */
package eu.letsplayonline.analytics.core.internal.events;

import eu.letsplayonline.analytics.core.internal.AnalyticsImpl;
import eu.letsplayonline.analytics.core.util.Entity;
import eu.letsplayonline.analytics.core.util.Values;

/**
 * This class handles events when a player says something.
 * 
 * @author Johannes Frank
 * 
 */
public class TalkEvent extends EventBase {

	public TalkEvent(Entity entity, String text, String channel,
			AnalyticsImpl base) {
		super(entity, base);
		addParam(Values.HIT_TYPE, "event");
		addParam(Values.EVENT_CATEGORY, "Player Chat");
		addParam(Values.EVENT_ACTION, channel);
		addParam(Values.EVENT_LABEL, "Chat message");
//		addParam(Values.EVENT_VALUE, text);
	}

}
