/**
 * 
 */
package eu.letsplayonline.analytics.core.internal.events;

import eu.letsplayonline.analytics.core.internal.AnalyticsImpl;
import eu.letsplayonline.analytics.core.util.Entity;
import eu.letsplayonline.analytics.core.util.Values;

/**
 * This class handles events when a player Dies
 * 
 * @author Johannes Frank
 * 
 */
public class DieEvent extends EventBase {

	public DieEvent(Entity entity, String cause, AnalyticsImpl base) {
		super(entity, base);
		addParam(Values.HIT_TYPE, "event");
		addParam(Values.EVENT_CATEGORY, "Player Death");
		addParam(Values.EVENT_ACTION, cause);
		addParam(Values.EVENT_LABEL, "Player died");
	}

}
