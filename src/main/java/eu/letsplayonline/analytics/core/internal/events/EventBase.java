package eu.letsplayonline.analytics.core.internal.events;

import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import eu.letsplayonline.analytics.core.internal.AnalyticsImpl;
import eu.letsplayonline.analytics.core.util.Entity;
import eu.letsplayonline.analytics.core.util.Values;

/**
 * General Event, takes care of connecting to google.
 * 
 * @author Johannes Frank
 * 
 */
public abstract class EventBase implements Runnable {

	/**
	 * The Map of Values to send in this event.
	 */
	private final Map<String, String> values = new HashMap<>();

	/**
	 * protected Constructor, only to be called by extended classes.
	 * 
	 * @param base
	 *            - the AnalyticsImpl instance that created this event.
	 * @param entity
     *            - todo document
	 */
	protected EventBase(Entity entity, AnalyticsImpl base) {
		addParam(Values.PROTOCOL_VERSION, Values.ENDPOINT_PROTOCOL_VERSION);
		addParam(Values.TRACKING_ID, base.getTrackerId());
		addParam(Values.DOCUMENT_LOCATION_URL, "http://" + base.getHostname()
				+ entity.getLocationNameAsUrl());
		addParam(Values.DOCUMENT_TITLE, entity.getLocationNameAugmented());
		addParam(Values.CLIENT_ID, entity.getId().toString());
		addParam(Values.IP_OVERRIDE, entity.getHost());
		addParam(Values.FLASH_VERSION, entity.getClientVersion());
	}

	/**
	 * This method adds a parameter key/value set to the parameter map. Once all
	 * parameters are given, these can be formed to one post string.
	 * 
	 * @param key
	 *            - the key
	 * @param value
	 *            - the value
	 * @return - this instance, for chain.calling.
	 */
	protected EventBase addParam(String key, String value) {
		values.put(key, value);
		return this;
	}

	@Override
	public void run() {
		HttpURLConnection connection = null;

		try {
			connection = (HttpURLConnection) new URL(Values.ENDPOINT)
					.openConnection();
			connection.setRequestProperty(
					"User-Agent",
					"Minecraft/5.0 (Linux; U; Intel Mac OS X 10.4; en-US; rv:"
							+ values.get(Values.FLASH_VERSION)
							+ ") Gecko/20100316 Minecraft/"
							+ values.get(Values.FLASH_VERSION));
			connection.setRequestMethod("POST");
			connection.setUseCaches(false);
			connection.setDoInput(true);
			connection.setDoOutput(true);

			// Send request
			OutputStreamWriter requestWriter = new OutputStreamWriter(
					connection.getOutputStream(), "UTF-8");
			// We serialize on the fly, then write the result into the stream.
			requestWriter.write(serializeParameters());
			requestWriter.flush();
			requestWriter.close();
			// Get the response code
			int responseCode = connection.getResponseCode();
		} catch (IOException uee) {
			uee.printStackTrace();
		} finally {
			// clean up.
			if (connection != null) {
				connection.disconnect();
			}
		}
	}

	/**
	 * This method serializes all the parameters that were previously given into
	 * the hashmap.
	 * 
	 * @return - a POST String representation of the parameters that were given.
	 * @throws UnsupportedEncodingException
	 *             - never, since UTF-8 is hardcoded.
	 */
	private String serializeParameters() throws UnsupportedEncodingException {
		StringBuilder sb = new StringBuilder();
		for (Entry<String, String> element : values.entrySet()) {
			sb.append(element.getKey()).append("=")
					.append(URLEncoder.encode(element.getValue(), "UTF-8"))
					.append("&");
		}
		String returnValue = sb.toString();
		// we use the substring here to get rid of the last &
		return returnValue.substring(0, returnValue.length() - 1);
	}
}
